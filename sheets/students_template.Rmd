---
title: "Übungszettel Vorlage"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Pakete laden
Laden Sie in diesem Code-Chunk alle Pakete, die sie benötigen. **Laden Sie Pakete immer am Anfang der Datei.**
```{r}

```


# Arbeitsverzeichnis setzen
Setzen Sie in diesem Code-Chunk das Arbeitsverzeichnis für diesen Übungszettel. **Setzen Sie das Arbeitsverzeichnis immer am Anfang der Datei** (vor oder nach den Paketen ist nicht wichtig.)
```{r}

```

# Aufgabe 1

Hier kommt Text hin.

```{r}
# Hier kommt Code hin.
```

Hier kann auch Text hin.

# Aufgabe 2

Hier kommt ihr Text hin.

```{r}
# Hier kommt ihr Code hin.
```

# Aufgabe 3

Hier kommt ihr Text hin.

```{r}
# Hier kommt ihr Code hin.
```

